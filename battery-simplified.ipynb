{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simplfied Battery \n",
    "\n",
    "This version is identical to battery.py but with simpler J and N (i.e. Neumann BCs)\n",
    "\n",
    "# Base PDE formulation\n",
    "\n",
    "We consider the PDE:\n",
    "\\begin{eqnarray*}\n",
    "\\frac{\\partial c}{\\partial t} - \\nabla \\cdot (\\textbf{A}(\\textbf{u}) \\nabla \\textbf{u}) &=& 0 \\quad \\text{in } \\Omega_a, \\Omega_e \\text{ and } \\Omega_c \\\\\n",
    "-\\nabla \\cdot ( \\textbf{B}(\\textbf{u}) \\nabla \\textbf{u}) &=& 0 \\quad \\text{in } \\Omega_a, \\Omega_e \\text{ and } \\Omega_c\n",
    "\\end{eqnarray*}\n",
    "Where $\\textbf{u} = (c, \\phi)$, i.e. the concentration and electric potential, $\\nabla \\textbf{u}$ is the Jacobian, and $\\textbf{A}(\\textbf{u}), \\textbf{B}(\\textbf{u})$ are row vectors given [here](#variables).\n",
    "\n",
    "The domain is given by a rectangle $\\Omega$ split into three parts. $\\Omega_a$ on left (anode), $\\Omega_e$ in middle (electrolyte), $\\Omega_c$ on right (cathode). The inner boundaries are $\\Gamma_a = \\bar \\Omega_a \\cap \\bar \\Omega_e$, $\\Gamma_c = \\bar \\Omega_e \\cap \\bar \\Omega_c$. The outer boundary is $\\Gamma_{out}$. Note that $\\textbf{u}$ is considered discontinuous across the inner boundaries, therefore we denote $\\textbf{u}$ separately as $\\textbf{u}_a, \\textbf{u}_e$ and $\\textbf{u}_c$ in each respective domain.\n",
    "\n",
    "For the outer boundary $\\Gamma_{out}$ we set the Neumann conditions (no flux),\n",
    "\\begin{eqnarray*}\n",
    "    (\\textbf{A}(\\textbf{u}) \\nabla \\textbf{u}) \\cdot \\textbf{n} = 0 \\\\\n",
    "    (\\textbf{B}(\\textbf{u}) \\nabla \\textbf{u}) \\cdot \\textbf{n} = 0\n",
    "\\end{eqnarray*}\n",
    "where $\\textbf{n}$ is the unit normal pointing out the domain. Additionally on the inner boundaries $\\Gamma_a$ we set the Neumann conditions,\n",
    "\\begin{eqnarray*}\n",
    "    (\\textbf{A}(\\textbf{u}_a) \\nabla \\textbf{u}_a) \\cdot \\textbf{n} = -(\\textbf{A}(\\textbf{u}_e) \\nabla \\textbf{u}_e) \\cdot \\textbf{n} &=& N(\\textbf{u}_{a}, \\textbf{u}_e) \\\\\n",
    "    (\\textbf{B}(\\textbf{u}_a) \\nabla \\textbf{u}_a) \\cdot \\textbf{n} = -(\\textbf{B}(\\textbf{u}_e) \\nabla \\textbf{u}_e) \\cdot \\textbf{n} &=& J(\\textbf{u}_{a}, \\textbf{u}_e)\n",
    "\\end{eqnarray*}\n",
    "and similarly for $\\Gamma_{c}$, where $N$ and $J$ are given [here](#boundary). Note that the negative sign on the $\\Omega_e$ side accounts for the fact the normal is inverted."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Weak formulation\n",
    "\n",
    "We will now write the weak form of the above PDE. Introducing a test function $\\textbf{v} = (v_0, v_1)$ and integrating over the domain, we get,\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega} \\frac{\\partial c}{\\partial t} v_0 - \\nabla \\cdot (\\textbf{A}(\\textbf{u}) \\nabla \\textbf{u}) v_0 \\ dV = 0 \\\\\n",
    "-\\int_{\\Omega}\\nabla \\cdot ( \\textbf{B}(\\textbf{u}) \\nabla \\textbf{u})v_1 \\ dV = 0\n",
    "\\end{eqnarray*}\n",
    "We then apply an implicit time discretisation, denoting the data from the previous time step by $\\textbf{u}^0 = (c^0, \\phi^0)$, the new time step by $\\textbf{u}^{1} = (c^{1}, \\phi^{1})$ and the size of the time step by $\\tau$. This gives us the following.\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega} c^0 v_0 \\ dV = \\int_{\\Omega} c^{1} v_0 - \\tau \\nabla \\cdot (\\textbf{A}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) v_0 \\ dV \\\\\n",
    "-\\int_{\\Omega}\\nabla \\cdot ( \\textbf{B}(\\textbf{u}^{1}) \\nabla \\textbf{u}^1)v_1 \\ dV = 0\n",
    "\\end{eqnarray*}\n",
    "We now apply Green's identity to get rid of the divergence terms. Note that for the following, we will separate the PDE into 3 equations for each part of the domain, since the boundaries are different in each case. For $\\Omega_a$, we get the following.\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega_a} c^0_a v_0 \\ dV = \\int_{\\Omega_a} c^{1}_a v_0 + \\tau (\\textbf{A}(\\textbf{u}^{1}_a) \\nabla \\textbf{u}^{1}_a) \\cdot \\nabla v_0 \\ dV - \\int_{\\Gamma_a} \\tau (\\textbf{A}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) \\cdot \\textbf{n} v_0 \\ dS \\\\\n",
    "\\int_{\\Omega_a} ( \\textbf{B}(\\textbf{u}^1_a) \\nabla \\textbf{u}^1_a)\\cdot \\nabla v_1 \\ dV  - \\int_{\\Gamma_a} \\tau (\\textbf{B}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) \\cdot \\textbf{n} v_1 \\ dS = 0\n",
    "\\end{eqnarray*}\n",
    "For $\\Omega_e$,\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega_e} c^0_e v_0 \\ dV = \\int_{\\Omega_e} c^{1}_e v_0 + \\tau (\\textbf{A}(\\textbf{u}^{1}_e) \\nabla \\textbf{u}^{1}_e) \\cdot \\nabla v_0 \\ dV - \\int_{\\Gamma_a \\cup \\Gamma_c} \\tau (\\textbf{A}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) \\cdot \\textbf{n} v_0 \\ dS \\\\\n",
    "\\int_{\\Omega_e} ( \\textbf{B}(\\textbf{u}^1_e) \\nabla \\textbf{u}^1_e) \\cdot \\nabla v_1 \\ dV - \\int_{\\Gamma_a \\cup \\Gamma_c} \\tau (\\textbf{B}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) \\cdot \\textbf{n} v_1 \\ dS = 0\n",
    "\\end{eqnarray*}\n",
    "And for $\\Omega_c$,\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega_c} c^0_c v_0 \\ dV = \\int_{\\Omega_a} c^{1}_c v_0 + \\tau (\\textbf{A}(\\textbf{u}^{1}_c) \\nabla \\textbf{u}^{1}_c) \\cdot \\nabla v_0 \\ dV - \\int_{\\Gamma_c} \\tau (\\textbf{A}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) \\cdot \\textbf{n} v_0 \\ dS \\\\\n",
    "\\int_{\\Omega_c} ( \\textbf{B}(\\textbf{u}^1_c) \\nabla \\textbf{u}^1_c)\\cdot \\nabla v_1 \\ dV - \\int_{\\Gamma_c} \\tau (\\textbf{B}(\\textbf{u}^{1}) \\nabla \\textbf{u}^{1}) \\cdot \\textbf{n} v_1 \\ dS = 0\n",
    "\\end{eqnarray*}\n",
    "Finally we apply our boundary conditions for the integrands with $dS$. For $\\Omega_a,$\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega_a} c^0_a v_0 \\ dV = \\int_{\\Omega_a} c^{1}_a v_0 + \\tau (\\textbf{A}(\\textbf{u}^{1}_a) \\nabla \\textbf{u}^{1}_a) \\cdot \\nabla v_0 \\ dV - \\int_{\\Gamma_a} \\tau N(\\textbf{u}^{1}_{a}, \\textbf{u}^{1}_e) v_0 \\ dS \\\\\n",
    "\\int_{\\Omega_a} ( \\textbf{B}(\\textbf{u}^1_a) \\nabla \\textbf{u}^1_a)\\cdot \\nabla v_1 \\ dV - \\int_{\\Gamma_a} J(\\textbf{u}^1_{a}, \\textbf{u}^1_e) v_1 \\ dS = 0\n",
    "\\end{eqnarray*}\n",
    "For $\\Omega_e$,\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega_e} c^0_e v_0 \\ dV= \\int_{\\Omega_e} c^{1}_e v_0 + \\tau (\\textbf{A}(\\textbf{u}^{1}_e) \\nabla \\textbf{u}^{1}_e) \\cdot \\nabla v_0 \\ dV + \\int_{\\Gamma_a} \\tau N(\\textbf{u}^{1}_a, \\textbf{u}^{1}_e) v_0 \\ dS+ \\int_{\\Gamma_c} \\tau N(\\textbf{u}^{1}_e, \\textbf{u}^{1}_c) v_0 \\ dS \\\\\n",
    "\\int_{\\Omega_e} ( \\textbf{B}(\\textbf{u}^1_e) \\nabla \\textbf{u}^1_e) \\cdot \\nabla v_1 \\ dV + \\int_{\\Gamma_a} J(\\textbf{u}^1_{a}, \\textbf{u}^1_e) v_1 \\ dS + \\int_{\\Gamma_c} J(\\textbf{u}^1_{e}, \\textbf{u}^1_c) v_1 \\ dS = 0\n",
    "\\end{eqnarray*}\n",
    "And for $\\Omega_c$,\n",
    "\\begin{eqnarray*}\n",
    "\\int_{\\Omega_c} c^0_c v_0 \\ dV = \\int_{\\Omega_a} c^{1}_c v_0 + \\tau (\\textbf{A}(\\textbf{u}^{1}_c) \\nabla \\textbf{u}^{1}_c) \\cdot \\nabla v_0 \\ dV - \\tau \\int_{\\Gamma_c} N(\\textbf{u}^{1}_e, \\textbf{u}^{1}_c) v_0 \\ dS \\\\\n",
    "\\int_{\\Omega_c} ( \\textbf{B}(\\textbf{u}^1_c) \\nabla \\textbf{u}^1_c)\\cdot \\nabla v_1 \\ dV - \\int_{\\Gamma_c} J(\\textbf{u}^1_{e}, \\textbf{u}^1_c) v_1 \\ dS = 0\n",
    "\\end{eqnarray*}\n",
    "\n",
    "We will now look at an implementation of the above in dune-fempy. First, let us import the necessary python modules."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from __future__ import print_function\n",
    "\n",
    "import math\n",
    "from ufl import *\n",
    "\n",
    "import dune.ufl\n",
    "import dune.fem\n",
    "\n",
    "import dune.create as create\n",
    "from dune.ufl import DirichletBC\n",
    "from dune.fem.view import filteredGridView"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us define the parameters for the problem. We take these from [here](#variables) and [here](#boundary)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "dune.fem.parameter.append(\"parameter\")\n",
    "\n",
    "# general parameters\n",
    "dimDomain = 2 \n",
    "dimRange = 2\n",
    "order = 1  # order of FE space\n",
    "numRefines = 1  # number of refinements of inital grid\n",
    "timeStep = 50  # size of timeStep\n",
    "maxIter = 20  # max number of solver iterations\n",
    "\n",
    "# problem parameters (from battery paper)\n",
    "R = 8.314\n",
    "T = 300\n",
    "F = 96485\n",
    "t_plus = [0, 0.2, 0]\n",
    "kappa = [1.0, 0.002, 0.038]\n",
    "D_e = [3.9e-10, 7.5e-7, e-9]\n",
    "c_init = [0.002639, 0.001, 0.020574]  # initial value for concentration\n",
    "phi_init = [0, 0, 0]  # initial value for potential"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the usual way we define the variables in UFL."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# define u^1 = (c^1, phi^1) and v = (v_0, v_1)\n",
    "uflSpace = dune.ufl.Space(dimDomain, dimRange)\n",
    "u = TrialFunction(uflSpace)\n",
    "v = TestFunction(uflSpace)\n",
    "\n",
    "# define un = (c^0, phi^0), u_a is for storing u\n",
    "# in Omega_a and so on for u_e and u_c\n",
    "un = Coefficient(uflSpace)\n",
    "u_a = Coefficient(uflSpace)\n",
    "u_e = Coefficient(uflSpace)\n",
    "u_c = Coefficient(uflSpace)\n",
    "dt = Constant(uflSpace.cell())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us define the PDE and boundary conditions (this is the main part to edit)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# define A_1 and A_2 in PDE (for the Id, a = 0, e = 1, c = 2)\n",
    "def A1(Id):\n",
    "    return D_e[Id] + R*T/(F**2)*t_plus[Id]**2*kappa[Id]/u[0]\n",
    "def A2(Id):\n",
    "    return kappa[Id]*t_plus[Id]/F\n",
    "\n",
    "# define B_1 and B_2 in PDE\n",
    "def B1(Id):\n",
    "    return R*T/F*t_plus[Id]*kappa[Id]/u[0]\n",
    "def B2(Id):\n",
    "    return kappa[Id]\n",
    "\n",
    "# define Neumann boundary term J for inner boundaries\n",
    "def J(uElec, uSolid):\n",
    "    return (uElec[0])**0.5*(uSolid[0])**0.5 *(exp(uElec[1] - uSolid[1]) \\\n",
    "                                             - exp(uSolid[1] - uElec[1]))\n",
    "\n",
    "# define dirichlet conditions on the left and right boundaries\n",
    "diric_a = [None, 2.5e-8]\n",
    "diric_c = [None, 1.9e-2]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We continue defining the PDE and boundary conditions (this part should remain the same)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# define J, N in Omega_a and Omega_c\n",
    "J_s = J(u_e, u)\n",
    "N_s = J_s/F\n",
    "# define J, N in Omega_e on Gamma_a and Gamma_c respectively\n",
    "J_ea = J(u, u_a)\n",
    "N_ea = J_ea/F\n",
    "J_ec = J(u, u_c)\n",
    "N_ec = J_ec/F\n",
    "\n",
    "# define the bilinear form's explicit part using B1, B2\n",
    "def eq_ex(Id):\n",
    "    ex = inner(un[0], v[0])*dx\n",
    "    ex += inner(B1(Id)*grad(u[0]) + B2(Id)*grad(u[1]), grad(v[1]))*dx\n",
    "    return ex\n",
    "# define the implicit part using A1, A2\n",
    "def eq_im(Id):\n",
    "    im = (inner(u[0], v[0]))*dx \n",
    "    im += dt*inner(A1(Id)*grad(u[0]) + A2(Id)*grad(u[1]), grad(v[0]))*dx\n",
    "    return im\n",
    "\n",
    "# let's combine the bilinear forms with the BCs in each domain\n",
    "a_ex = eq_ex(0) - J_s*v[1]*ds(4)\n",
    "a_im = eq_im(0) - dt*N_s*v[0]*ds(4)\n",
    "# same for Omega_e\n",
    "e_ex = eq_ex(1) + J_ec*v[1]*ds(5)\n",
    "e_im = eq_im(1) + dt*N_ea*v[0]*ds(3) + dt*N_ec*v[0]*ds(5)\n",
    "# and Omega_c\n",
    "c_ex = eq_ex(2) - J_s*v[1]*ds(4)\n",
    "c_im = eq_im(2) - dt*N_s*v[0]*ds(4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The remaining code is more or less trivial and should (usually) not need to be changed.\n",
    "\n",
    "Let us construct the three separate grids using a grid filter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def filter(e):\n",
    "    if e.geometry.center[0] <= 0.2:\n",
    "        return 3\n",
    "    elif 0.2 <= e.geometry.center[0] <= 0.8:\n",
    "        return 4\n",
    "    elif 0.8 <= e.geometry.center[0]:\n",
    "        return 5\n",
    "\n",
    "print('constructing grids')\n",
    "unitcube = 'unitcube-' + str(dimDomain) + 'd.dgf'\n",
    "#grid = create.view(\"adaptive\", create.grid(\"OneD\", unitcube))\n",
    "grid = create.view(\"adaptive\", create.grid(\"ALUCube\", unitcube, dimgrid=dimDomain))\n",
    "grid.hierarchicalGrid.globalRefine(numRefines)\n",
    "anode       = filteredGridView(grid, filter, 3)\n",
    "electrolyte = filteredGridView(grid, filter, 4)\n",
    "cathode     = filteredGridView(grid, filter, 5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We construct the FE spaces and the solutions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print('constructing spaces')\n",
    "space_a = create.space(\"Lagrange\", anode, dimrange=dimRange, order=order)\n",
    "space_e = create.space(\"Lagrange\", electrolyte, dimrange=dimRange, order=order)\n",
    "space_c = create.space(\"Lagrange\", cathode, dimrange=dimRange, order=order)\n",
    "\n",
    "print('constructing solutions')\n",
    "solution_a   = space_a.interpolate(lambda x: [c_init[0], phi_init[0]], name=\"solution_a\")\n",
    "solution_a_n = solution_a.copy()\n",
    "solution_a_n.assign( solution_a ) \n",
    "solution_e   = space_e.interpolate(lambda x: [c_init[1], phi_init[1]], name=\"solution_e\")\n",
    "solution_e_n = solution_e.copy()\n",
    "solution_e_n.assign( solution_e ) \n",
    "solution_c   = space_c.interpolate(lambda x: [c_init[2], phi_init[2]], name=\"solution_c\")\n",
    "solution_c_n = solution_c.copy()\n",
    "solution_c_n.assign( solution_c ) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We construct the models and schemes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# omega_a\n",
    "print('constructing models and schemes')\n",
    "model_a  = create.model(\"split\", anode, a_ex == a_im, \n",
    "    DirichletBC(uflSpace, diric_a, 6), coefficients={u_e: solution_e_n, un: solution_a_n})\n",
    "model_a.setConstant(dt, timeStep)\n",
    "scheme_a = create.scheme(\"h1\", space_a, model_a)\n",
    "\n",
    "# omega_e\n",
    "model_e  = create.model(\"split\", electrolyte, e_ex == e_im,\n",
    "    coefficients={u_a: solution_a_n, u_c: solution_c_n, un: solution_e_n})\n",
    "model_e.setConstant(dt, timeStep)\n",
    "scheme_e = create.scheme(\"h1\", space_e, model_e)\n",
    "\n",
    "# omega_c\n",
    "model_c  = create.model(\"split\", cathode, c_ex == c_im,\n",
    "    DirichletBC(uflSpace, diric_c, 7), coefficients={u_e: solution_e_n, un: solution_c_n})\n",
    "model_c.setConstant(dt, timeStep)\n",
    "scheme_c = create.scheme(\"h1\", space_c, model_c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we start the solving process over a loop."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "anode.writeVTK(\"battery_anode_\", pointdata=[solution_a], number=0)\n",
    "electrolyte.writeVTK(\"battery_electrolye_\", pointdata=[solution_e], number=0)\n",
    "cathode.writeVTK(\"battery_cathode_\", pointdata=[solution_c], number=0)\n",
    "for i in range(1, maxIter):\n",
    "    print('solving a, step: ', i)\n",
    "    scheme_a.solve(target=solution_a)\n",
    "    print('solving e, step: ', i)\n",
    "    scheme_e.solve(target=solution_e)\n",
    "    print('solving c, step: ', i)\n",
    "    scheme_c.solve(target=solution_c)\n",
    "    solution_a_n.assign(solution_a)\n",
    "    solution_e_n.assign(solution_e)\n",
    "    solution_c_n.assign(solution_c)\n",
    "    anode.writeVTK(\"battery_anode_\", pointdata=[solution_a], number=i)\n",
    "    electrolyte.writeVTK(\"battery_electrolye_\", pointdata=[solution_e], number=i)\n",
    "    cathode.writeVTK(\"battery_cathode_\", pointdata=[solution_c], number=i)\n",
    "print('finished')\n",
    "exit()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Details of PDE variables\n",
    "\n",
    "<a id='variables'></a>\n",
    "\n",
    "Here we will define the variables used in the formulation of the PDE.\n",
    "\n",
    "\\begin{gather*}\n",
    "\\textbf{A}(\\textbf{u}) = \\left(D_{e}(c, \\phi) + \\frac{R T}{F^2} \\frac{t_{+}^2 (c) \\kappa (c, \\phi)}{c}, \\kappa(c, \\phi) \\frac{t_+ (c)}{F}\\right) \\\\\n",
    "\\textbf{B}(\\textbf{u}) = \\left( \\frac{R T}{F} \\frac{t_{+} \\kappa(c, \\phi)}{c}, \\kappa(c, \\phi) \\right)\n",
    "\\end{gather*}\n",
    "where $F = 96485$ C mol$^{−1}$ is the Faraday constant, $R = 8.314$ J mol$^{−1}$ K$^{−1}$ is the gas constant and $T = 300$ K. $D_{e}$, $\\kappa$ and $t_+$ depend on the domain and are defined as follows\n",
    "\\begin{eqnarray*}\n",
    "\\begin{array}{cccc}\n",
    "\\text{Domain} & D_e & \\kappa & t_+ \\\\\n",
    "\\hline\n",
    "\\text{Anode} & 3.9 x 10^{-10} & 1.0 & 0 \\\\\n",
    "\\text{Electrolyte} & 7.5 x 10^{-7} & 0.002 & 0.2 \\\\\n",
    "\\text{Cathode} & 1.0 x 10^{-9} & 0.038 & 0\n",
    "\\end{array}\n",
    "\\end{eqnarray*}\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Details of boundary conditions\n",
    "\n",
    "<a id='boundary'></a>\n",
    "\n",
    "Recall that $\\textbf{u}$ is considered discontinuous across the inner boundaries, therefore we denote $\\textbf{u}$ separately as $\\textbf{u}_a, \\textbf{u}_e$ and $\\textbf{u}_c$ in each respective domain. We define $J$ and $N$ on $\\Gamma_a$ (and equivalently on $\\Gamma_c$) as follows \n",
    "\n",
    "\\begin{gather*}\n",
    "J(\\textbf{u}_{a}, \\textbf{u}_e) = c_e^{0.5} c_a^{0.5} \\left( \\exp \\left( \\phi_a - \\phi_e \\right) - \\exp \\left( \\phi_e - \\phi_a \\right) \\right) \\\\  \\\\\n",
    "N(\\textbf{u}_{a}, \\textbf{u}_e) = \\frac{J(\\textbf{u}_{a}, \\textbf{u}_e)}{F}\n",
    "\\end{gather*}\n",
    "\n",
    "this can potentially be rewritten as $J = J_1 J_2 J_3$ where \n",
    "\\begin{gather*}\n",
    "J_1 = c_e^{0.5}\\\\\n",
    "J_2 = c_a^{0.5}\\\\\n",
    "J_3 = \\left( \\exp \\left( \\phi_a - \\phi_e \\right) - \\exp \\left( \\phi_e - \\phi_a \\right) \\right)\n",
    "\\end{gather*}"
   ]
  }
 ],
 "metadata": {},
 "nbformat": 4,
 "nbformat_minor": 1
}
