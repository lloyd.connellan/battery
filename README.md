Discretisation of model from the paper: 'Finite Volume Discretization of Equations Describing Nonlinear Diffusion'

battery.ipynb: FEM continuous discretisation into three domains.
dg-battery.ipynb: discontinuous galerkin FEM discretisation for whole domain
