
# coding: utf-8

# # Simplfied Battery
#
# This version is identical to battery.py but with simpler J and N (i.e. Neumann BCs)
#
# # Base PDE formulation
#
# We consider the PDE:
# \begin{eqnarray*}
# \frac{\partial c}{\partial t} - \nabla \cdot (\textbf{A}(\textbf{u}) \nabla \textbf{u}) &=& 0 \quad \text{in } \Omega_a, \Omega_e \text{ and } \Omega_c \\
# -\nabla \cdot ( \textbf{B}(\textbf{u}) \nabla \textbf{u}) &=& 0 \quad \text{in } \Omega_a, \Omega_e \text{ and } \Omega_c
# \end{eqnarray*}
# Where $\textbf{u} = (c, \phi)$, i.e. the concentration and electric potential, $\nabla \textbf{u}$ is the Jacobian, and $\textbf{A}(\textbf{u}), \textbf{B}(\textbf{u})$ are row vectors given [here](#variables).
#
# The domain is given by a rectangle $\Omega$ split into three parts. $\Omega_a$ on left (anode), $\Omega_e$ in middle (electrolyte), $\Omega_c$ on right (cathode). The inner boundaries are $\Gamma_a = \bar \Omega_a \cap \bar \Omega_e$, $\Gamma_c = \bar \Omega_e \cap \bar \Omega_c$. The outer boundary is $\Gamma_{out}$. Note that $\textbf{u}$ is considered discontinuous across the inner boundaries, therefore we denote $\textbf{u}$ separately as $\textbf{u}_a, \textbf{u}_e$ and $\textbf{u}_c$ in each respective domain.
#
# For the outer boundary $\Gamma_{out}$ we set the Neumann conditions (no flux),
# \begin{eqnarray*}
#     (\textbf{A}(\textbf{u}) \nabla \textbf{u}) \cdot \textbf{n} = 0 \\
#     (\textbf{B}(\textbf{u}) \nabla \textbf{u}) \cdot \textbf{n} = 0
# \end{eqnarray*}
# where $\textbf{n}$ is the unit normal pointing out the domain. Additionally on the inner boundaries $\Gamma_a$ we set the Neumann conditions,
# \begin{eqnarray*}
#     (\textbf{A}(\textbf{u}_a) \nabla \textbf{u}_a) \cdot \textbf{n} = -(\textbf{A}(\textbf{u}_e) \nabla \textbf{u}_e) \cdot \textbf{n} &=& N(\textbf{u}_{a}, \textbf{u}_e) \\
#     (\textbf{B}(\textbf{u}_a) \nabla \textbf{u}_a) \cdot \textbf{n} = -(\textbf{B}(\textbf{u}_e) \nabla \textbf{u}_e) \cdot \textbf{n} &=& J(\textbf{u}_{a}, \textbf{u}_e)
# \end{eqnarray*}
# and similarly for $\Gamma_{c}$, where $N$ and $J$ are given [here](#boundary). Note that the negative sign on the $\Omega_e$ side accounts for the fact the normal is inverted.

# ## Weak formulation
#
# We will now write the weak form of the above PDE. Introducing a test function $\textbf{v} = (v_0, v_1)$ and integrating over the domain, we get,
# \begin{eqnarray*}
# \int_{\Omega} \frac{\partial c}{\partial t} v_0 - \nabla \cdot (\textbf{A}(\textbf{u}) \nabla \textbf{u}) v_0 \ dV = 0 \\
# -\int_{\Omega}\nabla \cdot ( \textbf{B}(\textbf{u}) \nabla \textbf{u})v_1 \ dV = 0
# \end{eqnarray*}
# We then apply an implicit time discretisation, denoting the data from the previous time step by $\textbf{u}^0 = (c^0, \phi^0)$, the new time step by $\textbf{u}^{1} = (c^{1}, \phi^{1})$ and the size of the time step by $\tau$. This gives us the following.
# \begin{eqnarray*}
# \int_{\Omega} c^0 v_0 \ dV = \int_{\Omega} c^{1} v_0 - \tau \nabla \cdot (\textbf{A}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) v_0 \ dV \\
# -\int_{\Omega}\nabla \cdot ( \textbf{B}(\textbf{u}^{1}) \nabla \textbf{u}^1)v_1 \ dV = 0
# \end{eqnarray*}
# We now apply Green's identity to get rid of the divergence terms. Note that for the following, we will separate the PDE into 3 equations for each part of the domain, since the boundaries are different in each case. For $\Omega_a$, we get the following.
# \begin{eqnarray*}
# \int_{\Omega_a} c^0_a v_0 \ dV = \int_{\Omega_a} c^{1}_a v_0 + \tau (\textbf{A}(\textbf{u}^{1}_a) \nabla \textbf{u}^{1}_a) \cdot \nabla v_0 \ dV - \int_{\Gamma_a} \tau (\textbf{A}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) \cdot \textbf{n} v_0 \ dS \\
# \int_{\Omega_a} ( \textbf{B}(\textbf{u}^1_a) \nabla \textbf{u}^1_a)\cdot \nabla v_1 \ dV  - \int_{\Gamma_a} \tau (\textbf{B}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) \cdot \textbf{n} v_1 \ dS = 0
# \end{eqnarray*}
# For $\Omega_e$,
# \begin{eqnarray*}
# \int_{\Omega_e} c^0_e v_0 \ dV = \int_{\Omega_e} c^{1}_e v_0 + \tau (\textbf{A}(\textbf{u}^{1}_e) \nabla \textbf{u}^{1}_e) \cdot \nabla v_0 \ dV - \int_{\Gamma_a \cup \Gamma_c} \tau (\textbf{A}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) \cdot \textbf{n} v_0 \ dS \\
# \int_{\Omega_e} ( \textbf{B}(\textbf{u}^1_e) \nabla \textbf{u}^1_e) \cdot \nabla v_1 \ dV - \int_{\Gamma_a \cup \Gamma_c} \tau (\textbf{B}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) \cdot \textbf{n} v_1 \ dS = 0
# \end{eqnarray*}
# And for $\Omega_c$,
# \begin{eqnarray*}
# \int_{\Omega_c} c^0_c v_0 \ dV = \int_{\Omega_a} c^{1}_c v_0 + \tau (\textbf{A}(\textbf{u}^{1}_c) \nabla \textbf{u}^{1}_c) \cdot \nabla v_0 \ dV - \int_{\Gamma_c} \tau (\textbf{A}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) \cdot \textbf{n} v_0 \ dS \\
# \int_{\Omega_c} ( \textbf{B}(\textbf{u}^1_c) \nabla \textbf{u}^1_c)\cdot \nabla v_1 \ dV - \int_{\Gamma_c} \tau (\textbf{B}(\textbf{u}^{1}) \nabla \textbf{u}^{1}) \cdot \textbf{n} v_1 \ dS = 0
# \end{eqnarray*}
# Finally we apply our boundary conditions for the integrands with $dS$. For $\Omega_a,$
# \begin{eqnarray*}
# \int_{\Omega_a} c^0_a v_0 \ dV = \int_{\Omega_a} c^{1}_a v_0 + \tau (\textbf{A}(\textbf{u}^{1}_a) \nabla \textbf{u}^{1}_a) \cdot \nabla v_0 \ dV - \int_{\Gamma_a} \tau N(\textbf{u}^{1}_{a}, \textbf{u}^{1}_e) v_0 \ dS \\
# \int_{\Omega_a} ( \textbf{B}(\textbf{u}^1_a) \nabla \textbf{u}^1_a)\cdot \nabla v_1 \ dV - \int_{\Gamma_a} J(\textbf{u}^1_{a}, \textbf{u}^1_e) v_1 \ dS = 0
# \end{eqnarray*}
# For $\Omega_e$,
# \begin{eqnarray*}
# \int_{\Omega_e} c^0_e v_0 \ dV= \int_{\Omega_e} c^{1}_e v_0 + \tau (\textbf{A}(\textbf{u}^{1}_e) \nabla \textbf{u}^{1}_e) \cdot \nabla v_0 \ dV + \int_{\Gamma_a} \tau N(\textbf{u}^{1}_a, \textbf{u}^{1}_e) v_0 \ dS+ \int_{\Gamma_c} \tau N(\textbf{u}^{1}_e, \textbf{u}^{1}_c) v_0 \ dS \\
# \int_{\Omega_e} ( \textbf{B}(\textbf{u}^1_e) \nabla \textbf{u}^1_e) \cdot \nabla v_1 \ dV + \int_{\Gamma_a} J(\textbf{u}^1_{a}, \textbf{u}^1_e) v_1 \ dS + \int_{\Gamma_c} J(\textbf{u}^1_{e}, \textbf{u}^1_c) v_1 \ dS = 0
# \end{eqnarray*}
# And for $\Omega_c$,
# \begin{eqnarray*}
# \int_{\Omega_c} c^0_c v_0 \ dV = \int_{\Omega_a} c^{1}_c v_0 + \tau (\textbf{A}(\textbf{u}^{1}_c) \nabla \textbf{u}^{1}_c) \cdot \nabla v_0 \ dV - \tau \int_{\Gamma_c} N(\textbf{u}^{1}_e, \textbf{u}^{1}_c) v_0 \ dS \\
# \int_{\Omega_c} ( \textbf{B}(\textbf{u}^1_c) \nabla \textbf{u}^1_c)\cdot \nabla v_1 \ dV - \int_{\Gamma_c} J(\textbf{u}^1_{e}, \textbf{u}^1_c) v_1 \ dS = 0
# \end{eqnarray*}
#
# We will now look at an implementation of the above in dune-fempy. First, let us import the necessary python modules.

# In[ ]:

from __future__ import print_function

import math
from ufl import *

import dune.ufl
import dune.fem

import dune.create as create
from dune.ufl import DirichletBC
from dune.fem.view import filteredGridView


# Let us define the parameters for the problem. We take these from [here](#variables) and [here](#boundary).

# In[ ]:

dune.fem.parameter.append("parameter")

# general parameters
dimDomain = 2
dimRange = 2
order = 1  # order of FE space
numRefines = 1  # number of refinements of inital grid
timeStep = 50  # size of timeStep
maxIter = 20  # max number of solver iterations

# problem parameters (from battery paper)
R = 8.314
T = 300
F = 96485
t_plus = [0, 0.2, 0]
kappa = [1.0, 0.002, 0.038]
D_e = [3.9e-10, 7.5e-7, e-9]
c_init = [0.002639, 0.001, 0.020574]  # initial value for concentration
phi_init = [0, 0, 0]  # initial value for potential


# In the usual way we define the variables in UFL.

# In[ ]:

# define u^1 = (c^1, phi^1) and v = (v_0, v_1)
uflSpace = dune.ufl.Space(dimDomain, dimRange)
u = TrialFunction(uflSpace)
v = TestFunction(uflSpace)

# define un = (c^0, phi^0), u_a is for storing u
# in Omega_a and so on for u_e and u_c
un = Coefficient(uflSpace)
u_a = Coefficient(uflSpace)
u_e = Coefficient(uflSpace)
u_c = Coefficient(uflSpace)
dt = Constant(uflSpace.cell())


# Let us define the PDE and boundary conditions (this is the main part to edit).

# In[ ]:

# define A_1 and A_2 in PDE (for the Id, a = 0, e = 1, c = 2)
def A1(Id):
    return D_e[Id] + R*T/(F**2)*t_plus[Id]**2*kappa[Id]/u[0]
def A2(Id):
    return kappa[Id]*t_plus[Id]/F

# define B_1 and B_2 in PDE
def B1(Id):
    return R*T/F*t_plus[Id]*kappa[Id]/u[0]
def B2(Id):
    return kappa[Id]

# define Neumann boundary term J for inner boundaries
def J(uElec, uSolid):
    return (uElec[0])**0.5*(uSolid[0])**0.5 *(exp(uElec[1] - uSolid[1])                                              - exp(uSolid[1] - uElec[1]))

# define dirichlet conditions on the left and right boundaries
diric_a = [None, 2.5e-8]
diric_c = [None, 1.9e-2]


# We continue defining the PDE and boundary conditions (this part should remain the same).

# In[ ]:

# define J, N in Omega_a and Omega_c
J_s = J(u_e, u)
N_s = J_s/F
# define J, N in Omega_e on Gamma_a and Gamma_c respectively
J_ea = J(u, u_a)
N_ea = J_ea/F
J_ec = J(u, u_c)
N_ec = J_ec/F

# define the bilinear form's explicit part using B1, B2
def eq_ex(Id):
    ex = inner(un[0], v[0])*dx
    ex += inner(B1(Id)*grad(u[0]) + B2(Id)*grad(u[1]), grad(v[1]))*dx
    return ex
# define the implicit part using A1, A2
def eq_im(Id):
    im = (inner(u[0], v[0]))*dx
    im += dt*inner(A1(Id)*grad(u[0]) + A2(Id)*grad(u[1]), grad(v[0]))*dx
    return im

# let's combine the bilinear forms with the BCs in each domain
a_ex = eq_ex(0) - J_s*v[1]*ds(4)
a_im = eq_im(0) - dt*N_s*v[0]*ds(4)
# same for Omega_e
e_ex = eq_ex(1) + J_ec*v[1]*ds(5)
e_im = eq_im(1) + dt*N_ea*v[0]*ds(3) + dt*N_ec*v[0]*ds(5)
# and Omega_c
c_ex = eq_ex(2) - J_s*v[1]*ds(4)
c_im = eq_im(2) - dt*N_s*v[0]*ds(4)


# The remaining code is more or less trivial and should (usually) not need to be changed.
#
# Let us construct the three separate grids using a grid filter.

# In[ ]:

def filter(e):
    if e.geometry.center[0] <= 0.2:
        return 3
    elif 0.2 <= e.geometry.center[0] <= 0.8:
        return 4
    elif 0.8 <= e.geometry.center[0]:
        return 5

print('constructing grids')
unitcube = 'unitcube-' + str(dimDomain) + 'd.dgf'
#grid = create.view("adaptive", create.grid("OneD", unitcube))
grid = create.view("adaptive", create.grid("ALUCube", unitcube, dimgrid=dimDomain))
grid.hierarchicalGrid.globalRefine(numRefines)
anode       = filteredGridView(grid, filter, 3)
electrolyte = filteredGridView(grid, filter, 4)
cathode     = filteredGridView(grid, filter, 5)


# We construct the FE spaces and the solutions.

# In[ ]:

print('constructing spaces')
space_a = create.space("Lagrange", anode, dimrange=dimRange, order=order)
space_e = create.space("Lagrange", electrolyte, dimrange=dimRange, order=order)
space_c = create.space("Lagrange", cathode, dimrange=dimRange, order=order)

print('constructing solutions')
solution_a   = space_a.interpolate(lambda x: [c_init[0], phi_init[0]], name="solution_a")
solution_a_n = solution_a.copy()
solution_a_n.assign( solution_a )
solution_e   = space_e.interpolate(lambda x: [c_init[1], phi_init[1]], name="solution_e")
solution_e_n = solution_e.copy()
solution_e_n.assign( solution_e )
solution_c   = space_c.interpolate(lambda x: [c_init[2], phi_init[2]], name="solution_c")
solution_c_n = solution_c.copy()
solution_c_n.assign( solution_c )


# We construct the models and schemes.

# In[ ]:

# omega_a
print('constructing models and schemes')
model_a  = create.model("split", anode, a_ex == a_im,
    DirichletBC(uflSpace, diric_a, 6), coefficients={u_e: solution_e_n, un: solution_a_n})
model_a.setConstant(dt, timeStep)
scheme_a = create.scheme("h1", space_a, model_a)

# omega_e
model_e  = create.model("split", electrolyte, e_ex == e_im,
    coefficients={u_a: solution_a_n, u_c: solution_c_n, un: solution_e_n})
model_e.setConstant(dt, timeStep)
scheme_e = create.scheme("h1", space_e, model_e)

# omega_c
model_c  = create.model("split", cathode, c_ex == c_im,
    DirichletBC(uflSpace, diric_c, 7), coefficients={u_e: solution_e_n, un: solution_c_n})
model_c.setConstant(dt, timeStep)
scheme_c = create.scheme("h1", space_c, model_c)


# Finally we start the solving process over a loop.

# In[ ]:

anode.writeVTK("battery_anode_", pointdata=[solution_a], number=0)
electrolyte.writeVTK("battery_electrolye_", pointdata=[solution_e], number=0)
cathode.writeVTK("battery_cathode_", pointdata=[solution_c], number=0)
for i in range(1, maxIter):
    print('solving a, step: ', i)
    scheme_a.solve(target=solution_a)
    print('solving e, step: ', i)
    scheme_e.solve(target=solution_e)
    print('solving c, step: ', i)
    scheme_c.solve(target=solution_c)
    solution_a_n.assign(solution_a)
    solution_e_n.assign(solution_e)
    solution_c_n.assign(solution_c)
    anode.writeVTK("battery_anode_", pointdata=[solution_a], number=i)
    electrolyte.writeVTK("battery_electrolye_", pointdata=[solution_e], number=i)
    cathode.writeVTK("battery_cathode_", pointdata=[solution_c], number=i)
print('finished')
exit()


# ## Details of PDE variables
#
# <a id='variables'></a>
#
# Here we will define the variables used in the formulation of the PDE.
#
# \begin{gather*}
# \textbf{A}(\textbf{u}) = \left(D_{e}(c, \phi) + \frac{R T}{F^2} \frac{t_{+}^2 (c) \kappa (c, \phi)}{c}, \kappa(c, \phi) \frac{t_+ (c)}{F}\right) \\
# \textbf{B}(\textbf{u}) = \left( \frac{R T}{F} \frac{t_{+} \kappa(c, \phi)}{c}, \kappa(c, \phi) \right)
# \end{gather*}
# where $F = 96485$ C mol$^{−1}$ is the Faraday constant, $R = 8.314$ J mol$^{−1}$ K$^{−1}$ is the gas constant and $T = 300$ K. $D_{e}$, $\kappa$ and $t_+$ depend on the domain and are defined as follows
# \begin{eqnarray*}
# \begin{array}{cccc}
# \text{Domain} & D_e & \kappa & t_+ \\
# \hline
# \text{Anode} & 3.9 x 10^{-10} & 1.0 & 0 \\
# \text{Electrolyte} & 7.5 x 10^{-7} & 0.002 & 0.2 \\
# \text{Cathode} & 1.0 x 10^{-9} & 0.038 & 0
# \end{array}
# \end{eqnarray*}
#

# ## Details of boundary conditions
#
# <a id='boundary'></a>
#
# Recall that $\textbf{u}$ is considered discontinuous across the inner boundaries, therefore we denote $\textbf{u}$ separately as $\textbf{u}_a, \textbf{u}_e$ and $\textbf{u}_c$ in each respective domain. We define $J$ and $N$ on $\Gamma_a$ (and equivalently on $\Gamma_c$) as follows
#
# \begin{gather*}
# J(\textbf{u}_{a}, \textbf{u}_e) = c_e^{0.5} c_a^{0.5} \left( \exp \left( \phi_a - \phi_e \right) - \exp \left( \phi_e - \phi_a \right) \right) \\  \\
# N(\textbf{u}_{a}, \textbf{u}_e) = \frac{J(\textbf{u}_{a}, \textbf{u}_e)}{F}
# \end{gather*}
#
# this can potentially be rewritten as $J = J_1 J_2 J_3$ where
# \begin{gather*}
# J_1 = c_e^{0.5}\\
# J_2 = c_a^{0.5}\\
# J_3 = \left( \exp \left( \phi_a - \phi_e \right) - \exp \left( \phi_e - \phi_a \right) \right)
# \end{gather*}
