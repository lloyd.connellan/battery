
# coding: utf-8

# # Simple test case (scalar version)
#
# Let us test a simplified version of the problem given in battery.ipynb. Let us consider a domain with only two parts, $\Omega_a$, and $\Omega_b$, with inner boundary $\Gamma$.
#
# Here we will just look at a scalar problem, duplicated in each domain, with boundary conditions that rely on the other domain. i.e. we have the following
#
# \begin{gather}
# - \nabla (A(u_a) \nabla u_a) = f_A \quad \text{in } \Omega_a \\
# - \nabla (B(u_a) \nabla u_B) = f_B \quad \text{in } \Omega_b
# \end{gather}
#
# Discretising this will give us the following
# \begin{gather}
# \int_{\Omega_a} A(u_a) \nabla u_a \cdot \nabla v \ dV - \int_{\Gamma} N(u_{a}, u_b) v \ dS = \int_{\Omega_a} f_A v \ dV \\
# \int_{\Omega_b} B(u_b) \nabla u_b \cdot \nabla v \ dV - \int_{\Gamma} N(u_{a}, u_b) v \ dS = \int_{\Omega_b} f_B v \ dV
# \end{gather}

# In[ ]:

from __future__ import print_function

import math
from ufl import *

import dune.ufl
import dune.fem

import dune.create as create
from dune.fem.view import filteredGridView


# Set up model

# In[ ]:

dimDomain = 2
dimRange = 1
deltaT = 0.01

uflSpace = dune.ufl.Space(dimDomain, dimRange)
u = TrialFunction(uflSpace)
v = TestFunction(uflSpace)
u_a = Coefficient(uflSpace) # u in \Omega_a
u_b = Coefficient(uflSpace) # u in \Omega_b
x = SpatialCoordinate(uflSpace.cell())

alpha = 10
A = 1
B = cos(0.5)/alpha

C = cos(0.5)/(sin(0.5) - alpha/4)
N_a = (u - u_b)*C
N_b = (u_a - u)*C
f_a = sin(x[0])*x[1]*x[1] - 2*sin(x[0])
f_b = -2*cos(0.5)*(x[0]*x[0] + x[1]*x[1])

a_a = A*inner(grad(u), grad(v))*dx - inner(N_a, v)*ds(1)
L_a = f_a*v[0]*dx
a_b = B*inner(grad(u), grad(v))*dx + inner(N_b, v)*ds(1)
L_b = f_b*v[0]*dx

exact_a = as_vector([ sin(x[0])*x[1]*x[1] ])
exact_b = as_vector([ x[0]*x[0]*x[1]*x[1]*10. ])


# Set up grid

# In[ ]:

order = 1
grid  = create.view("adaptive", create.grid("ALUConform", "unitcube-2d.dgf", dimgrid=dimDomain))
grid.hierarchicalGrid.globalRefine(3)


# Set up the main method. `grid` is split into ($\Omega_a$ and $\Omega_b$), and the schemes are set up and solved.

# In[ ]:

gridA = filteredGridView(grid, lambda e: e.geometry.center[0] <= 0.5, useFilteredIndexSet=True)
gridB = filteredGridView(grid, lambda e: e.geometry.center[0] >= 0.5, useFilteredIndexSet=True)
#fullgrid = filteredGridView(grid, lambda e: True, useFilteredIndexSet=True)
print('grid filters constructed')

spcA  = create.space("Lagrange", gridA, dimrange=dimRange, order=order)
spcB  = create.space("Lagrange", gridB, dimrange=dimRange, order=order)
#fullspc  = create.space("Lagrange", fullgrid, dimrange=dimRange, order=order)
print('spaces set up')

exactA = create.function("ufl", gridA, "exact_a", 5, exact_a) # exact solution for u_a defined in \Omega_a
exactB = create.function("ufl", gridB, "exact_b", 5, exact_b)
solutionA  = spcA.interpolate(exactA, name="solutionA")
solutionB  = spcB.interpolate(exactB, name="solutionB")
#fullexactA = spcA.interpolate(exactA, name="exactA")
#fullexactB = spcB.interpolate(exactB, name="exactB")
print('exact solution function created')

#from dune.models.elliptic import generateModel
#generateModel(gridA, a_a == L_a, dirichlet={2:exact_a}, header='createdmodel_A.hh', modelType='split')
#generateModel(gridB, a_b == L_b, dirichlet={2:exact_b}, header='createdmodel_B.hh', modelType='split')

modelA  = create.model("split", gridA, a_a == L_a, coefficients={u_b:solutionB}, dirichlet={2:exact_a})
modelB  = create.model("split", gridB, a_b == L_b, coefficients={u_a:solutionA}, dirichlet={2:exact_b})
#modelA  = create.model("split", gridA, "createdmodel_A.hh", coefficients={u_b:solutionB})
#modelB  = create.model("split", gridB, "createdmodel_B.hh", coefficients={u_a:solutionA})
print('models set up')

schemeA = create.scheme("h1", spcA, modelA)
schemeB = create.scheme("h1", spcB, modelB)
print('schemes set up')

for i in range(30):
    schemeA.solve(target=solutionA)
    print('first equation solved')
    schemeB.solve(target=solutionB)
    print('second equation solved')

    def l2error_a(en,x):
        val = solutionA.localFunction(en).evaluate(x) - exactA.localFunction(en).evaluate(x)
        return [ val[0]*val[0] ];
    l2error_gf_a = create.function("local", gridA, "error", 5, l2error_a)
    def l2error_b(en,x):
        val = solutionB.localFunction(en).evaluate(x) - exactB.localFunction(en).evaluate(x)
        return [ val[0]*val[0] ];
    l2error_gf_b = create.function("local", gridB, "error", 5, l2error_b)
    error_a = sqrt(l2error_gf_a.integrate()[0])
    error_b = sqrt(l2error_gf_b.integrate()[0])

    print("size:", grid.size(0), "L2-error for a:", error_a, "L2-error for b:", error_b)
    gridA.writeVTK("battery-testA", pointdata=[solutionA, l2error_gf_a], number=i)
    gridB.writeVTK("battery-testB", pointdata=[solutionB, l2error_gf_b], number=i)
